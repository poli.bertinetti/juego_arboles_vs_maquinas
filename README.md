2017

Autores: Gonzalez Budiño Joaquin, Leopoldo Bertinetti

Contact: 
Joaquin - joa_gzb@hotmail.com

Concepto
===========
Combatir la deforestación.

Descripción
============
Juego casual-estratégico similar al juego de mesa Teg en el que se desea recuperar las zonas deforestadas plantando nuevos árboles. Estas zonas siguen estando bajo el dominio de empresas que destruyen este medio ambiente por lo que las mismas corren riesgo de ser arrasadas nuevamente.

Objetivos
==========
Defender los bosques y hacerlos crecer en las distintas zonas recuperadas.

Enemigos
=========
Maquinarias que se acercan desde diferentes puntos con el objetivo de talar los árboles. Cada maquinaria tendrá su forma de ataque.

Protagonistas
==============
Diferentes actores del medio ambiente: árboles y animales. Éstos combaten y se resisten a las maquinarias mediante determinadas acciones propias.

Victoria
==========
El juego termina al conquistar todas las áreas (niveles).

Derrota
========
Se pierde el juego cuando se pierden todas las zonas de bosques.


Mecánica del juego
===================

Mapas estratégicos (escena principal)
--------------------------------------
El nivel de dificultad de cada zona estará definida por la “fuerza de resistencia” de nuestros recursos y además de una dificultad ya establecida. Por cada zona conquistada nuestra fuerza se incrementará y al mismo tiempo la “fuerza de oposición” del enemigo.

Zona de guerra (escena secundaria)
-----------------------------------
El jugador posee recursos iniciales limitados (y materia prima limitada) y los puede ubicar de antemano en dicha zona. También existirá la posibilidad de agregar nuevos recursos una vez iniciada la partida.
Si nos destruyen un recurso, podemos volver a ubicar otro en su lugar.
Si nos destruyen todos los recursos en la zona, se pierde el juego.
La zona se dividirá en 4x4 casilleros. Cada recurso podrá ser ubicado sólo en un casillero. Estos casilleros, tanto horizontalmente como verticalmente, servirán como vías para el movimiento de las maquinarias.
Se debe resistir 1 minuto manteniendo como mínimo 1 recurso (conquista la zona).
Si una maquinaria pasa por una vía y no se encuentra con ningún recurso, ésta al finalizar el recorrido se destruye.
Cada zona tendrá cierta cantidad de enemigos, que se distribuirán aleatoriamente por las distintas direcciones.
Una vez iniciada la partida, los recursos tendrán un tiempo de aparición (momentáneamente 10).
Por cada enemigo destruido, se obtiene como recompensa materia prima para nuevos recursos.
Con la materia prima se obtienen los recursos.


Recursos del jugador
=====================
Cada tipo de árbol o animal tendrá puntos de resistencia y ataque (1 a 10).
Posibles tipos: roble, jaguar, pino, yacaré

Roble:
Ataque: 1 casillero hacia todas las direcciones (sin diagonales)


Enemigos
==========
Cada tipo de enemigo tendrá puntos de ataque y resistencia (1 a 10).
Cada enemigo aparecerá cada 5 segundos, momentáneamente.
Tipos: leñador con hacha, leñador con motosierra, tractor, máquina grande.

Formas para administrar los recursos
=====================================

Idea 1: Los enemigo incrementan puntos cuando se los mata. Estos puntos serviran, en el mapa estratégico para ser intercambiados por materia prima.

Idea 2: la materia prima sobrante de una zona, podrá utilizarse en otra zona.
