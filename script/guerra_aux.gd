extends Sprite

onready var global = get_node("/root/global");
onready var lab_puntaje = get_node("Panel/lab_puntaje");
onready var lab_agua = get_node("Panel/lab_agua");
onready var lab_comida = get_node("Panel/lab_comida");
onready var panel = get_node("Panel");
onready var contenedor = get_node("contenedor");

############################# banderas ###################################
var preparacion; #bandera que indica el momento de preparacion antes de que vengan los enemigos


#################### constantes y variables #####################################
const tam_map = 5; #4 carriles cada lado
var reloj_casero; #cuenta regresiva para hacer aparecer un enemigo
var pantalla; #tamano de la pantalla de guerra

var cant_enemigos; #controla la cantidad de enemigos que hay en el mapa (cuando se anula, se termina la partida)
var cant_amigos; #controla la cantidad de amigos que hay en el mapa (cuando se anula, se termina la partida)
var nro_enemigos; #cantidad de enemigos que vana aparecer. puede ser cualquier tipo

var grid_map = [];
var matriz_pos=[];
var columnas = 5;
var filas = 5;
var unitBoundedToMouse  = null;
var mouseClickPosition  = null;

################################# func ready ###########################################
func _ready():
	randomize();
	preparacion = true;
	generate_grid_map();
	pantalla = get_viewport_rect().size;
	nro_enemigos = global.nro_enemigos;
	reloj_casero = 2;
	cant_enemigos=0;
	cant_amigos=0;
	global.set_pantalla_size(pantalla,panel.get_size());
	lab_agua.set_text("Agua:" + str(global.agua));
	lab_comida.set_text("comida:" + str(global.comida));
	lab_puntaje.set_text("puntaje: " + str(global.puntaje));
	var pant = pantalla / tam_map;
	for j in range (1, tam_map +1):
		for i in range(1, tam_map +1):
			get_node("p" + st(i)).set_pos(Vector2(i));
	set_fixed_process(true);
	set_process_input(true);


func _fixed_process(delta):
	reloj_casero -= delta; #reloj hecho a pata
	game_over();
	if (reloj_casero<1 and nro_enemigos>0 and cant_amigos>0):
		preparacion=false;
		spawn_enemy(0); ####solo generar hachadores, mejorar y ver esto en script global
		nro_enemigos -= 1;
		reloj_casero = 2;


################################# funciones ##########################################
func spawn_enemy(ocurrencia): #hace aparecer enemigos al azar en el mapa de guerra
	if (ocurrencia == 0):
		var char = preload("res://escenas/hachador.tscn").instance(); #cargar instancia del hachador
		var pos_spawn = pantalla / tam_map;
		var carril = 1+randi()%4;
		var desdeDondeViene = 10 + randi()%4;
		char.set_DesdeDondeViene(desdeDondeViene); #esto se debe hacer mediante una funcion para un buen diseno
		if (desdeDondeViene == 10): #arriba
			char.set_pos(Vector2(carril*pos_spawn.x, 0+panel.get_size().y));
		if (desdeDondeViene == 11): #derecha
			char.set_pos(Vector2(pantalla.x, carril*pos_spawn.y));
		if (desdeDondeViene == 12): #abajo
			char.set_pos(Vector2(carril*pos_spawn.x, pantalla.y));
		if (desdeDondeViene == 13): #izquierda
			char.set_pos(Vector2(0, carril*pos_spawn.y));
		add_child(char);
		char.add_to_group("enemigos");
		char.connect("muerto",self,"enemigo_muerto"); #si se muere un enemigo, hay que decrementar el contador de su cantidad
		char.connect(char.get_name(),self,"recolectar_puntos",[char.get_puntaje()]);
		cant_enemigos += 1;


func generate_grid_map():
	for x in range(0, columnas + 1):
		grid_map.append([]);
		for y in range(0, filas + 1):
			grid_map[x].append(0);


func amigo_muerto(): #si a un aliado lo matan, decrementar la cantidad de amigos en el mapa (funcion de senal)
	cant_amigos -= 1;


func enemigo_muerto(): #si el enemigo sale de pantalla o lo matan, decrementar la cantidad de enemigos en el mapa (funcion de senal)
	cant_enemigos -= 1;


func recolectar_puntos(p): #cada enemigo devuelve un puntaje cuando se lo mata (funcion de senal)
	global.puntaje += p;
	lab_puntaje.set_text("puntaje: "+str(global.puntaje));


func game_over():#fin de juego cuando se mueren todos los enemigos o cuando mueren todos los arboles
	if( (cant_enemigos==0 or cant_amigos==0) and preparacion==false):
		if(cant_amigos==0):
			global.set_victoria(false);
		else:
			global.set_victoria(true);
		get_tree().change_scene("res://escenas/mapa_global.tscn");
		self.queue_free();



func superposicion( pos ):
	var bandera = false;
	for node in get_tree().get_nodes_in_group("amigos"):
		if (node.get_pos() == pos ):
			bandera = true;
	return bandera;


func _on_planta1_button_up():
	var char = preload("res://escenas/arbol.tscn").instance();
	if (global.agua>=char.agua):
		var posx = get_global_mouse_pos().x
		var posy = get_global_mouse_pos().y
		if(superposicion(Vector2(posx,posy))==false):
			char.set_pos(Vector2(posx,posy));
			add_child(char);
			char.add_to_group("amigos");
			char.connect("muerto",self,"amigo_muerto");
			global.agua -= char.agua;
			lab_agua.set_text("Agua:" + str(global.agua));
			cant_amigos += 1;