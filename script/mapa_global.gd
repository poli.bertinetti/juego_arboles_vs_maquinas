extends Control

onready var global = get_node("/root/global");
# Copyright 2017 Joaquin Gonzalez Budiño, Leopoldo Bertinetti
#
#This file is part of Árboles vs Máquinas.
#
#    Árboles vs Máquinas is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    Árboles vs Máquinas is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with Árboles vs Máquinas.  If not, see <http://www.gnu.org/licenses/>.


onready var lab_puntaje = get_node("recursos/puntaje");
onready var lab_comida  = get_node("recursos/comida");
onready var lab_agua = get_node("recursos/agua");
onready var aviso_faltarecurso = get_node("ConfirmationDialog 2");

var dos_v; #para efecto de parpadeo
var turno; #a quien le toca jugar? 1->jugador, -1->maquina. se calcula tirando dados

var mapa_en_juego; #variable que guarda en que zona se va a pelear en cierto instante
var perdiste; #bandera que indica con verdadero si se perdio el juego

func _ready():
	global.dificultad();
	perder();
	ganar();
	turno = global.jugando;
	mostrar_boton_defender();
	mostrar_spr_conquistados();
	mostrar_etiqueta_dificultad();
	get_node("recursos").set_hidden(false);
	get_node("TextureFrame/pasar_turno").set_hidden(true);
	get_node("TextureFrame/pasar_turno").set_disabled(true);
	lab_puntaje.set_text("puntaje: " + str(global.puntaje));
	lab_comida.set_text("comida: " + str(global.comida));
	lab_agua.set_text("agua: " + str(global.agua));
	dos_v=0.5;
	turno_maquina();
	print("zonas conquistadas: " + str(global.cant_zonas_conquistadas))
	set_process(true);


func _process(delta):
	dos_v -= delta;
	if(dos_v<=-0.5): dos_v=0.5;
	atenuar_boton(delta,dos_v);


#################################### efecto parpadeo ###############################################
func atenuar_boton(delta,d):
	#cuando es el turno del jugador, se habilitan todos los botones
	if(turno==1):
		if(global.cant_zonas_conquistadas>0):
			get_node("TextureFrame/pasar_turno").set_hidden(false);
			get_node("TextureFrame/pasar_turno").set_disabled(false);
	#efecto de parpadeo para comprar agua y comida cuando hay puntos suficientes
		var aux1=get_node("recursos/plus_agua");
		if(global.puntaje<global.costo_agua):
			get_node("recursos/boton_agua").set_disabled(true);
			aux1.set_hidden(true);
		else:
			get_node("recursos/boton_agua").set_disabled(false);
			aux1.set_hidden(false);
			if(d>=0): aux1.set_opacity(1);
			else: aux1.set_opacity(0);
		var aux2 = get_node("recursos/plus_comida");
		if(global.puntaje<global.costo_comida):
			get_node("recursos/boton_comida").set_disabled(true);
			aux2.set_hidden(true);
		else:
			get_node("recursos/boton_comida").set_disabled(false);
			aux2.set_hidden(false);
			if(d>=0): aux2.set_opacity(1);
			else: aux2.set_opacity(0);
	else:
		get_node("TextureFrame/pasar_turno").set_hidden(true);
		get_node("recursos/plus_agua").set_hidden(true);
		get_node("recursos/plus_comida").set_hidden(true);

 ###################################opciones en el turno del jugador ###########################
func mostrar_boton_defender():#cuando se ha conquistado una region, saco el boton de defenderla
	if(turno==1):
		var aux=global.mapa.size();
		for i in range(0,aux):
			var cadena = "TextureFrame/zona" + str(i) +"/boton_defender" + str(i);
			if(global.mapa[i]==1):
				get_node(cadena).set_hidden(true);
			else:
				get_node(cadena).set_hidden(false);
	else:
		var aux=global.mapa.size();
		for i in range(0,aux):
			var cadena = "TextureFrame/zona" + str(i) +"/boton_defender" + str(i);
			get_node(cadena).set_hidden(true);


func mostrar_spr_conquistados(): #cuando se conquista una zona, se marca con un tilde
	for i in range(0,global.mapa.size()):
		var cadena = "TextureFrame/zona" + str(i) +"/Spr_conquistado";
		if(global.mapa[i]==1):
			get_node(cadena).set_hidden(false);
		else:
			get_node(cadena).set_hidden(true);


func mostrar_etiqueta_dificultad(): #indica que tan dificil(en cantidad y tipo de enemigos) sera la ronda actual
	var nota = "facil";
	if(global.nro_enemigos > 15 && global.nro_enemigos < 20): nota = "moderado";
	elif(global.nro_enemigos > 22): nota = "Dificil";
	get_node("recursos/lab_dificultad").set_text("Dificultad: " + nota);


####################################### botones panel jugador #############################################
func _on_boton_comida_pressed():
	global.comprar_materiaPrima(100);
	lab_puntaje.set_text("puntaje: " + str(global.puntaje));
	lab_comida.set_text("comida: " + str(global.comida));


func _on_boton_agua_pressed():
	global.comprar_materiaPrima(101);
	lab_puntaje.set_text("puntaje: " + str(global.puntaje));
	lab_agua.set_text("agua: " + str(global.agua));



func _on_pasar_turno_pressed(): #dar la jugabilidad a la maquina
	global.pasar_turno();
	_ready();



################################# botones de defender #############################################
func _on_boton_defender0_pressed(): #luchar por una region para conquistarla
	global.set_mapa_activo(0);
	mapa_en_juego = 0;
	if(global.check_materiaPrima()==true):
		get_tree().change_scene("res://escenas/guerra.tscn");
	else:
		aviso_faltarecurso.set_hidden(false);

func _on_boton_defender1_pressed():
	global.set_mapa_activo(1);
	mapa_en_juego = 1;
	if(global.check_materiaPrima()==true):
		get_tree().change_scene("res://escenas/guerra.tscn");
	else:
		aviso_faltarecurso.set_hidden(false);

func _on_boton_defender2_pressed():
	global.set_mapa_activo(2);
	mapa_en_juego = 2;
	if(global.check_materiaPrima()==true):
		get_tree().change_scene("res://escenas/guerra.tscn");
	else:
		aviso_faltarecurso.set_hidden(false);


func _on_boton_defender3_pressed():
	global.set_mapa_activo(3);
	mapa_en_juego = 3;
	if(global.check_materiaPrima()==true):
		get_tree().change_scene("res://escenas/guerra.tscn");
	else:
		aviso_faltarecurso.set_hidden(false);



################################# jugabilidad ##########################################
func turno_maquina(): #la maquina decide atacar algun mapa que el jugador haya conquistado
	if(turno == -1):
		var aviso = get_node("ConfirmationDialog");
		aviso.set_hidden(false);
		mapa_en_juego = global.mapa.find(1);
		global.set_mapa_activo(mapa_en_juego);
		aviso.set_text("El enemigo quiere retomar la zona " + str(mapa_en_juego)+"\n ¿Desea defender la zona?");
		aviso.get_cancel().connect("pressed",self,"_Confirm_dialog_cancel");


func _Confirm_dialog_cancel():
	global.set_victoria(false);
	perdiste=perder();
	_ready();


func _on_ConfirmationDialog_confirmed(): #cuando se confirma defender una zona ya tomada, se pasa al juego
	global.set_mapa_activo(mapa_en_juego);
	if(global.check_materiaPrima()==true):
		get_tree().change_scene("res://escenas/guerra.tscn");
	else:
		aviso_faltarecurso.set_hidden(false);
		turno_maquina();




func ganar(): #una vez que se conquista todo el mapa, el juego se termina y mostrar mensaje de felicitaciones
	if(perdiste == true):
		var aviso_gameover = get_node("WindowDialog");
		set_fixed_process(false);
		aviso_gameover.set_hidden(false);
		aviso_gameover.set_text("HAS PERDIDO");
	elif(global.cant_zonas_conquistadas==global.mapa.size() && perdiste==false):
		var aviso_gameover = get_node("WindowDialog");
		set_fixed_process(false);
		aviso_gameover.set_hidden(false);



func _on_WindowDialog_confirmed():
	get_tree().quit();


func perder():#cambia la bandera perdiste si es que esta todo conquistado o no queda manera de defenderse por falta de materia prima
	perdiste=global.perder();





############################## sonidos #########################################
func _on_ConfirmationDialog_about_to_show(): #efecto de sonido cuando aparece pop-up que la maquina ataca
	pass # replace with function body

func _on_WindowDialog_about_to_show(): ##sonido cuando se gana o pierde
	pass # replace with function body


