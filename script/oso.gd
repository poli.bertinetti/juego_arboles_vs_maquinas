# Copyright 2017 Joaquin Gonzalez Budiño, Leopoldo Bertinetti
#
#This file is part of Árboles vs Máquinas.
#
#    Árboles vs Máquinas is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    Árboles vs Máquinas is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with Árboles vs Máquinas.  If not, see <http://www.gnu.org/licenses/>.

extends KinematicBody2D

onready var tiempo = self.get_node("Timer");
onready var tiempo2 = self.get_node("Timer 2");
onready var anim = get_node("AnimationPlayer");

############################### atributos del oso ##################################
var resistencia = 10; #vida
const time_ataque =2.5;
const ataque = 3; #puntos de ataque
const comida=3; #costo de llamar a un oso

################################# estados/banderas ###########################################
var atacando;
var AquienAtaca;
signal muerto;

################################ loop principal de eventos #################################
func _ready():
	atacando = false;
	AquienAtaca = null;
	tiempo.set_active(true); #activar reloj
	tiempo2.set_active(true); #activar reloj
	tiempo.set_wait_time(time_ataque);
	tiempo2.set_wait_time(1);
	set_fixed_process(true);


func _fixed_process(delta):
	morir();
	if(atacando==true):
		atacar(AquienAtaca);


################################## acciones propias ########################################
func morir(): #si pierde toda tiene que desaparecer
	if(resistencia<1):
		emit_signal("muerto");
		set_fixed_process(false);
		queue_free();


func recibir_danio(cant): #quitarle vida porque es atacado
	resistencia -= cant;
	return resistencia;


func atacar(body):
	var node_ref = weakref(body);
	if node_ref.get_ref()!=null:
		anim.play("atacar");
		body.recibir_danio(ataque); #vida_cuerpo es la vida que le resta al oponente despues de haber sido atacado
		tiempo.start();
	else:
		AquienAtaca = null;
		atacando = false;
		tiempo.stop();



func enemigo_a_vista():
	if(AquienAtaca==null):
		var enemies = get_node("Area2D").get_overlapping_bodies();
		if(enemies.empty()==false):
			while((enemies.empty()==false) and (enemies.front().is_in_group("amigos"))):
				enemies.pop_front();
			if(enemies.empty()==false):
				AquienAtaca = enemies.front();
				atacando = true;
			else:
				tiempo.stop();
				atacando = false;
				AquienAtaca = null;



######################################### eventos #############################################
func _on_Timer_timeout():
	atacando = true;


func _on_Timer_2_timeout(): #comprobar cada 1 segundo si hay algun enemigo a la vista
	enemigo_a_vista();
