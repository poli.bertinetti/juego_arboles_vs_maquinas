# Copyright 2017 Joaquin Gonzalez Budiño, Leopoldo Bertinetti
#
#This file is part of Árboles vs Máquinas.
#
#    Árboles vs Máquinas is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    Árboles vs Máquinas is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with Árboles vs Máquinas.  If not, see <http://www.gnu.org/licenses/>.

extends Control



func _ready():
	pass

func _on_play_pressed():
	get_tree().change_scene("res://escenas/mapa_global.tscn")
