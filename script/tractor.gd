# Copyright 2017 Joaquin Gonzalez Budiño, Leopoldo Bertinetti
#
#This file is part of Árboles vs Máquinas.
#
#    Árboles vs Máquinas is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    Árboles vs Máquinas is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with Árboles vs Máquinas.  If not, see <http://www.gnu.org/licenses/>.

extends Node2D

onready var pantalla = get_viewport_rect().size;
onready var textura = self.get_node("Sprite");
onready var tiempo = self.get_node("Timer");


############################### atributos del tractor ##################################
var resistencia = 20; #vida
const ataque = 4; #puntos de ataque
const move_velocidad = 14; #que tan rapido se mueve
const time_ataque = 3; #lapso de tiempo entre ataques de este enemigo
const puntaje = 4;


############################# estados y variables globales #####################################
var atacando;
var moviendo;
var DesdeDondeViene;
var AquienAtaca;
signal muerto;

################################ loop principal de eventos #################################
func _ready():
	atacando = false;
	moviendo = true;
	AquienAtaca = null;
	tiempo.set_active(true); #activar reloj
	tiempo.set_wait_time(time_ataque);
	self.add_user_signal(get_name()); #agregar una señal con el mismo nombre que este nodo, para manejar el puntaje (si este muere,avisar para sumar puntos)
	set_fixed_process(true);


func _fixed_process(delta):
	morir(); #checkear constantemente si esta muerto
	if(moviendo==true && atacando==false): #se queda quieto atacando o se mueve y no ataca
		movimiento(delta);
	if(moviendo==false && atacando==true):
		atacando = false;
		atacar(AquienAtaca);



##################### acciones propias de este enemigo #################################
func movimiento(delta): #moverlo
	var motion;
	if (DesdeDondeViene == 10):
		motion = Vector2(0,1);
		move( delta*move_velocidad*motion );
	if (DesdeDondeViene == 11):
		motion = Vector2(-1,0);
		move( delta*move_velocidad*motion );
	if (DesdeDondeViene == 12):
		motion = Vector2(0,-1);
		move( delta*move_velocidad*motion );
	if (DesdeDondeViene == 13):
		motion = Vector2(1,0);
		move( delta*move_velocidad*motion );


func recibir_danio(cant): #quitarle vida porque es atacado, y devolver la cantidad de vida que le queda a este personaje
	resistencia -= cant;
	return resistencia;


func atacar(body):
	var node_ref = weakref(body);
	if node_ref.get_ref()!=null:
		body.recibir_danio(ataque); #vida_cuerpo es la vida que le resta al oponente despues de haber sido atacado
		tiempo.start();
	else:
		AquienAtaca = null;
		tiempo.stop();
		moviendo = true;
		atacando = false;


func desde_donde_venis():
	return DesdeDondeViene;

func get_puntaje():
	return puntaje;

func set_DesdeDondeViene(v):
	DesdeDondeViene=v;


############################### eventos #############################################
func morir(): #si pierde toda la vida o sale de pantalla, tiene que desaparecer
	var tam_textura = textura.get_texture().get_size()*textura.get_scale() / 2;
	var afuera =  pantalla - self.get_pos();
	if(resistencia<1 or afuera.x<0-tam_textura.x or afuera.y<0-tam_textura.y or afuera.x>pantalla.x+tam_textura.x or afuera.y>pantalla.y-global.banda.y):
		if(resistencia<1):
			emit_signal(self.get_name());
		emit_signal("muerto");
		set_fixed_process(false);
		queue_free();



func enemigo_a_vista(): #si un enemigo tiene en frente a los buenos, este debe atacar
	if(AquienAtaca==null):
		var enemies = get_node("Area2D").get_overlapping_bodies();
		if(enemies.empty()==false):
			while((enemies.empty()==false) and (enemies.front().is_in_group("enemigos"))):
				enemies.pop_front();
			if(enemies.empty()==false):
				AquienAtaca = enemies.front();
				moviendo = false;
				atacando = true;
			else:
				tiempo.stop();
				moviendo = true;
				atacando = false;
				AquienAtaca = null;


#atacar cada 2.5 segundos activando una bandera
func _on_Timer_timeout():
	atacando = true;


func _on_Timer_2_timeout():
	enemigo_a_vista();


