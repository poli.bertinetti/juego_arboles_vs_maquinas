# Copyright 2017 Joaquin Gonzalez Budiño, Leopoldo Bertinetti
#
#This file is part of Árboles vs Máquinas.
#
#    Árboles vs Máquinas is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    Árboles vs Máquinas is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with Árboles vs Máquinas.  If not, see <http://www.gnu.org/licenses/>.

extends StaticBody2D

onready var tiempo = self.get_node("Timer");
onready var tiempo2 = self.get_node("Timer 2");


############################### atributos del arbol ##################################
var resistencia = 6; #vida
const time_ataque =1.5;
const ataque = 1; #puntos de ataque del pajaro que crea (solo como info - aca no se usa esta var)
const agua=2; #costo de plantar este arbol

################################# estados/banderas ###########################################
var atacando;
var dir_disparo;
signal muerto;
var desde_donde_viene_enemigo;

################################ loop principal de eventos #################################
func _ready():
	atacando = false;
	tiempo.set_active(true); #activar reloj
	tiempo2.set_active(true); #activar reloj
	tiempo.set_wait_time(time_ataque);
	tiempo2.set_wait_time(1);
	set_fixed_process(true);


func _fixed_process(delta):
	morir();
	if(atacando==true):
		atacar();


################################## acciones propias ########################################
func morir(): #si pierde toda tiene que desaparecer
	if(resistencia<1):
		emit_signal("muerto");
		set_fixed_process(false);
		queue_free();


func recibir_danio(cant): #quitarle vida porque es atacado
	resistencia -= cant;
	return resistencia;


func atacar(): #el arbol ataca desde lejos enviando pajaros al ataque
	var pajaro = preload("res://escenas/shoot_pajaro.tscn").instance(); #cargar instancia del pajaro
	pajaro.set_pos(get_node("Position2D").get_pos());
	pajaro.hacia_donde_volar(desde_donde_viene_enemigo);
	pajaro.move_direccion(dir_disparo);
	add_child(pajaro);
	atacando = false;
	tiempo.start();


func direccion_disparo(e):
	dir_disparo = Vector2(e.get_pos().x-self.get_pos().x,e.get_pos().y-self.get_pos().y);
	desde_donde_viene_enemigo = e.desde_donde_venis();


func enemigo_a_vista(): #si se encuentra un enemigo a la vista, atacar
	var enemies = get_node("Area2D").get_overlapping_bodies();
	if(enemies.empty()==false):
		while((enemies.empty()==false) and (enemies.front().is_in_group("amigos"))):
			enemies.pop_front();
		if(enemies.empty()==false):
			var e = enemies.front();
			direccion_disparo(e);
			atacando = true;
		else:
			atacando = false;
			tiempo.stop();
	else:
		atacando = false;
		tiempo.stop();



######################################### eventos #############################################
func _on_Timer_timeout():
	atacando = true;


func _on_Timer_2_timeout(): #comprobar cada 1 segundo si hay algun enemigo a la vista
	enemigo_a_vista();


