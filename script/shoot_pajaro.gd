# Copyright 2017 Joaquin Gonzalez Budiño, Leopoldo Bertinetti
#
#This file is part of Árboles vs Máquinas.
#
#    Árboles vs Máquinas is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    Árboles vs Máquinas is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with Árboles vs Máquinas.  If not, see <http://www.gnu.org/licenses/>.

extends Area2D

const speed = 750;
const ataque =1;

var dir; #hacia donde vuela el pajaro

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	destruir()
	mover(delta);



func destruir():
	if(dir==Vector2(0,0) or dir==null):
		set_fixed_process(false);
		queue_free();

func hacia_donde_volar(desde_donde_viene_enemigo):
	if(desde_donde_viene_enemigo==13):
		self.get_node("Sprite").set_flip_h(false);


func mover(delta):
	translate(delta*speed*dir.normalized());



func move_direccion(dirXY): #el arbol detecta desde donde viene el enemigo, entonces dispara hacia la dir del enemigo
	dir = dirXY;


func _on_shoot_pajaro_body_enter( body ):
	if (body.is_in_group("enemigos")==true):
		if(body.has_method("recibir_danio")):
			body.recibir_danio(ataque);
			set_fixed_process(false);
			queue_free();


func _on_Timer_timeout():
	set_fixed_process(false);
	queue_free();
