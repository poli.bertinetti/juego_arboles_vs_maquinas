# Copyright 2017 Joaquin Gonzalez Budiño, Leopoldo Bertinetti
#
#This file is part of Árboles vs Máquinas.
#
#    Árboles vs Máquinas is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    Árboles vs Máquinas is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with Árboles vs Máquinas.  If not, see <http://www.gnu.org/licenses/>.


extends Node2D

onready var global = get_node("/root/global");
onready var lab_puntaje = get_node("Panel/lab_puntaje");
onready var lab_agua = get_node("Panel/lab_agua");
onready var lab_comida = get_node("Panel/lab_comida");
onready var panel = get_node("Panel");

############################# banderas ###################################
var preparacion; #bandera que indica el momento de preparacion antes de que vengan los enemigos


#################### constantes y variables #####################################
const tam_map = 5; #4 carriles cada lado
var reloj_casero; #cuenta regresiva para hacer aparecer un enemigo
var pantalla; #tamano de la pantalla de guerra

var cant_enemigos; #controla la cantidad de enemigos que hay en el mapa (cuando se anula, se termina la partida)
var cant_amigos; #controla la cantidad de amigos que hay en el mapa (cuando se anula, se termina la partida)
var nro_enemigos; #cantidad de enemigos que vana aparecer. puede ser cualquier tipo

var ocupado_map = [];
var matriz_pos=[];
var columnas = tam_map;
var filas = tam_map;


################################# func ready ###########################################
func _ready():
	randomize();
	preparacion = true;
	generate_grid_map();
	pantalla = get_viewport_rect().size;
	reloj_casero = 2;
	cant_enemigos=0;
	cant_amigos=0;
	global.set_pantalla_size(pantalla,panel.get_size());
	lab_agua.set_text("Agua:" + str(global.agua));
	lab_comida.set_text("comida:" + str(global.comida));
	lab_puntaje.set_text("puntaje: " + str(global.puntaje));
	var pant = pantalla / tam_map;
	var cont=1;
	for j in range(1,columnas):
		for i in range (1,filas):
			get_node("background/p"+str(cont)).set_pos(Vector2(i*pant.x,j*pant.y));
			matriz_pos.push_front(get_node("background/p"+str(cont)));
			cont+=1;
	nro_enemigos = global.nro_enemigos;
	set_fixed_process(true);


func _fixed_process(delta):
	reloj_casero -= delta; #reloj hecho a pata
	game_over();
	if (reloj_casero<0 and nro_enemigos>0 and cant_amigos>0):
		var f =randi()%2;
		spawn_enemy(f);
		reloj_casero = 2;


################################# funciones ##########################################
func spawn_enemy(ocurrencia): #hace aparecer enemigos al azar en el mapa de guerra
	if (ocurrencia == 0):
		if(global.cuales_enemigos[0]>0):
			preparacion=false;
			var char = preload("res://escenas/hachador.tscn").instance(); #cargar instancia del hachador
			var pos_spawn = pantalla / tam_map;
			var carril = 1+randi()%4;
			var desdeDondeViene = 10 + randi()%4;
			char.set_DesdeDondeViene(desdeDondeViene); #esto se debe hacer mediante una funcion para un buen diseno
			if (desdeDondeViene == 10): #arriba
				char.set_pos(Vector2(carril*pos_spawn.x, 0+panel.get_size().y));
			if (desdeDondeViene == 11): #derecha
				char.set_pos(Vector2(pantalla.x, carril*pos_spawn.y));
			if (desdeDondeViene == 12): #abajo
				char.set_pos(Vector2(carril*pos_spawn.x, pantalla.y));
			if (desdeDondeViene == 13): #izquierda
				char.set_pos(Vector2(0, carril*pos_spawn.y));
			add_child(char);
			char.add_to_group("enemigos");
			char.connect("muerto",self,"enemigo_muerto"); #si se muere un enemigo, hay que decrementar el contador de su cantidad
			char.connect(char.get_name(),self,"recolectar_puntos",[char.get_puntaje()]);
			cant_enemigos += 1;
			nro_enemigos -=1;
			global.decrementar_enemigo(0);
	if(ocurrencia == 1):
		if(global.cuales_enemigos[1]>0):
			preparacion=false;
			var char = preload("res://escenas/tractor.tscn").instance(); #cargar instancia del hachador
			var pos_spawn = pantalla / tam_map;
			var carril = 1+randi()%4;
			var desdeDondeViene = 10 + randi()%4;
			char.set_DesdeDondeViene(desdeDondeViene); #esto se debe hacer mediante una funcion para un buen diseno
			if (desdeDondeViene == 10): #arriba
				char.set_pos(Vector2(carril*pos_spawn.x, 0+panel.get_size().y));
			if (desdeDondeViene == 11): #derecha
				char.set_pos(Vector2(pantalla.x, carril*pos_spawn.y));
			if (desdeDondeViene == 12): #abajo
				char.set_pos(Vector2(carril*pos_spawn.x, pantalla.y));
			if (desdeDondeViene == 13): #izquierda
				char.set_pos(Vector2(0, carril*pos_spawn.y));
			add_child(char);
			char.add_to_group("enemigos");
			char.connect("muerto",self,"enemigo_muerto"); #si se muere un enemigo, hay que decrementar el contador de su cantidad
			char.connect(char.get_name(),self,"recolectar_puntos",[char.get_puntaje()]);
			cant_enemigos += 1;
			global.decrementar_enemigo(1);
			nro_enemigos -=1;





func generate_grid_map(): #matriz que contiene 1 si un lugar esta ocupado
	for x in range(0, columnas*filas-1):
		ocupado_map.append(0);




func amigo_muerto(pos): #si a un aliado lo matan, decrementar la cantidad de amigos en el mapa (funcion de senal)
	cant_amigos -= 1;
	var p = 0;
	for i in range(1,matriz_pos.size()):
		if(Vector2(pos.x,pos.y).distance_to(Vector2(matriz_pos[i].get_pos().x,matriz_pos[i].get_pos().y))<Vector2(pos.x,pos.y).distance_to(Vector2(matriz_pos[p].get_pos().x,matriz_pos[p].get_pos().y))):
			p=i;
	print(p)
	ocupado_map[p]=0;


func enemigo_muerto(): #si el enemigo sale de pantalla o lo matan, decrementar la cantidad de enemigos en el mapa (funcion de senal)
	cant_enemigos -= 1;


func recolectar_puntos(p): #cada enemigo devuelve un puntaje cuando se lo mata (funcion de senal)
	global.puntaje += p;
	lab_puntaje.set_text("puntaje: "+str(global.puntaje));


func game_over():#fin de juego cuando se mueren todos los enemigos o cuando mueren todos los arboles
	if( (cant_enemigos==0 and nro_enemigos<=0 and preparacion==false) or (cant_amigos==0 and preparacion==false)):
		if(cant_amigos==0):
			global.set_victoria(false);
		else:
			global.set_victoria(true);
		get_tree().change_scene("res://escenas/mapa_global.tscn");
		self.queue_free();



func superposicion( p ):
	if(ocupado_map[p]==1):
		return true;
	else:
		return false;


func _on_planta1_button_up():
	var char = preload("res://escenas/arbol.tscn").instance();
	if (global.agua>=char.agua):
		var posx = get_global_mouse_pos().x
		var posy = get_global_mouse_pos().y

		var ubicacion = Vector2(matriz_pos[0].get_pos().x,matriz_pos[0].get_pos().y);
		var p = 0;
		for i in range(1,matriz_pos.size()):
			if(Vector2(posx,posy).distance_to(Vector2(matriz_pos[i].get_pos().x,matriz_pos[i].get_pos().y))<Vector2(posx,posy).distance_to(ubicacion)):
				ubicacion = Vector2(matriz_pos[i].get_pos().x,matriz_pos[i].get_pos().y);
				p=i;
		if(superposicion(p)==false):
			char.set_pos(ubicacion);
			ocupado_map[p]=1;
			add_child(char);
			char.add_to_group("amigos");
			char.connect("muerto",self,"amigo_muerto",[char.get_pos()]);
			global.agua -= char.agua;
			lab_agua.set_text("Agua:" + str(global.agua));
			cant_amigos += 1;


func _on_animal1_button_up():
	var char = preload("res://escenas/oso.tscn").instance();
	if (global.comida>=char.comida):
		var posx = get_global_mouse_pos().x
		var posy = get_global_mouse_pos().y

		var ubicacion = Vector2(matriz_pos[0].get_pos().x,matriz_pos[0].get_pos().y);
		var p = 0;
		for i in range(1,matriz_pos.size()):
			if(Vector2(posx,posy).distance_to(Vector2(matriz_pos[i].get_pos().x,matriz_pos[i].get_pos().y))<Vector2(posx,posy).distance_to(ubicacion)):
				ubicacion = Vector2(matriz_pos[i].get_pos().x,matriz_pos[i].get_pos().y);
				p=i;
		if(superposicion(p)==false):
			char.set_pos(ubicacion);
			ocupado_map[p]=1;
			add_child(char);
			char.add_to_group("amigos");
			char.connect("muerto",self,"amigo_muerto",[char.get_pos()]);
			global.comida -= char.comida;
			lab_comida.set_text("comida:" + str(global.comida));
			cant_amigos += 1;
