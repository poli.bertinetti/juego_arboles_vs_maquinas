extends Node


################################recursos de jugador a lo largo del juego #############################
onready var agua = 6;
onready var comida = 4;
onready var puntaje = 0;


const costo_agua = 2;
const costo_comida = 4;


#en base a esto se calcula la dificultad, a mayor nivel, mas dificil
onready var victorias = 0;
onready var derrotas = 0;
onready var nivel = 1;


var mapa_activo;
var mapa=[0,0,0,0]; #almacena un 1 cuando una zona es conquistada y 0 cuando se ha perdido
var nro_enemigos; #se guardan la cantidad de enemigos que van a aparecer
var cuales_enemigos=[]; #decide que cantidad de cada enemigo apareceran. pos0=hachador, pos1=tractor
var cant_zonas_conquistadas =0;

onready var jugando=1; #determina de quien es el turno:: 1->jugador, (-1)->maquina

var pantalla;
var banda;

signal win;

################################ control detras del juego ##################################
func set_pantalla_size(tam,b):
	banda = b;
	pantalla = tam;



func comprar_materiaPrima(queCosa): #cambiar puntos por agua o comida
	if(puntaje>0):
		if(queCosa==100): #queCosa=100:comida
			if(puntaje>=costo_comida):
				puntaje -= costo_comida;
				comida += 1;
		elif(queCosa==101): #queCosa=101:comida
			if(puntaje>=costo_agua):
				puntaje -= costo_agua;
				agua += 1;


func dificultad(): #establece la dificultad de un nivel, cambiando nro_enemigos y cuales_enemigos
	var dados_maquina = 1;
	if(nivel<=1):
		cuales_enemigos = [10,0];
		nro_enemigos=10;
	else:
		nro_enemigos=8;
		dados_maquina += abs(victorias-derrotas);
		for i in range(1,dados_maquina):
			nro_enemigos += 1+randi()%7;
		var azar = (victorias-derrotas) / nivel;
		if (azar<=0): azar=0.95;
		var c = floor(azar*nro_enemigos);
		cuales_enemigos = [c,(nro_enemigos-c)*nivel];


func decrementar_enemigo(p):
	cuales_enemigos[p] -= 1;


func pasar_turno():
	jugando = -1;


func descontar_puntos(cant):
	puntaje -= cant;



func incrementar_nivel(): #bandera en true si se gana un nivel
		nivel += 1;


func incrementar_victoria():
	victorias += 1;


func incrementar_derrota():
	derrotas += 1;


func set_victoria(bandera):
	if(mapa_activo!=-1):
		if(bandera==true):
			mapa[mapa_activo]=1;
			incrementar_victoria();
			if(jugando==1):
				cant_zonas_conquistadas += 1;
		else:
			mapa[mapa_activo]=0;
			incrementar_derrota();
			cant_zonas_conquistadas -= 1;
	if(jugando==1): jugando = -1;
	else: jugando=1;
	incrementar_nivel();
	mapa_activo = -1;


func set_mapa_activo(ref):
	mapa_activo=ref;

func check_materiaPrima():
	if((agua>1 or comida>2)):
		return true;
	else:
		return false;



func perder():#cambia la bandera perdiste si es que esta todo conquistado o no queda manera de defenderse por falta de materia prima
	if(cant_zonas_conquistadas<0 or (puntaje<2 && agua<2)):
		return true;
	else:
		return false;
